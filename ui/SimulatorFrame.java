package ro.tuc.pt.asig2.ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class SimulatorFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JLabel lblServer;
    public static JTextField txtServers;
    private JLabel lblTasks;
    public static JTextField txtTasks;
    private JLabel lblTime;
    public static JTextField txtTime;
    private JLabel lblSpawn;
    public static JTextField txtSpawn;
    public static JButton btnStart;
    private static JTextArea txtLog;
    public static JButton btnStop;
    private JLabel lblSel;
    public static JComboBox<String> cmbSel;
    private JScrollPane jsp;
    
	public SimulatorFrame(){
        
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		String[] cmbSelItems = {"Shortest Queue", "Shortest Waiting Time"};
		
		//construct components
        lblServer = new JLabel ("Number of Servers");
        txtServers = new JTextField ("3");
        lblTasks = new JLabel ("Number of Tasks");
        txtTasks = new JTextField ("100");
        lblTime = new JLabel ("Max Processing Time");
        txtTime = new JTextField ("10");
        lblSpawn = new JLabel ("Max Arrival Time");
        txtSpawn = new JTextField ("100");
        btnStart = new JButton ("Start Simulation");
        btnStop = new JButton ("Stop Simulation");
        txtLog = new JTextArea (5, 5);
        jsp = new JScrollPane(txtLog, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        lblSel = new JLabel ("Selection Type");
        cmbSel = new JComboBox<String> (cmbSelItems);

      //set components properties
        txtLog.setEnabled (false);
        btnStop.setEnabled (false);

        //adjust size and set layout
        this.setSize(686, 500);
        this.setLayout (null);

        //add components
        add (lblServer);
        add (txtServers);
        add (lblTasks);
        add (txtTasks);
        add (lblTime);
        add (txtTime);
        add (lblSpawn);
        add (txtSpawn);
        add (btnStart);
        add (btnStop);
        add (lblSel);
        add (cmbSel);
        add (jsp);

        //set component bounds (only needed by Absolute Positioning)
        btnStart.setBounds (10, 305, 155, 30);
        btnStop.setBounds (10, 345, 155, 30);
        txtServers.setBounds (10, 40, 155, 25);
        txtTasks.setBounds (10, 95, 155, 25);
        lblServer.setBounds (15, 15, 120, 25);
        lblTasks.setBounds (15, 70, 105, 25);
        lblTime.setBounds (15, 126, 136, 25);
        txtTime.setBounds (10, 152, 155, 25);
        lblSpawn.setBounds (15, 179, 136, 25);
        txtSpawn.setBounds (10, 204, 155, 25);
        lblSel.setBounds (15, 232, 100, 25);
        cmbSel.setBounds (10, 256, 155, 25);
        jsp.setBounds (180, 15, 480, 435);
        
        
        txtLog.setLineWrap(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(dim.width/2-500, dim.height/2-this.getSize().height/2);
		this.setTitle("Server Simulator");
		this.setVisible(true);

	}
	
	public static void setLog (final String txt){
		 SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                txtLog.setText( txt );
	                
	            }
	        });

	}
	
}
