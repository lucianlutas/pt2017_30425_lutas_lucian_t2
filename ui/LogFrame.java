package ro.tuc.pt.asig2.ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class LogFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private static JTextArea txtLog;
	private JScrollPane jsp;
	
	public LogFrame() {
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
        //construct components
        txtLog = new JTextArea (0, 0);
        jsp = new JScrollPane(txtLog, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        //set components properties
        txtLog.setEnabled (false);

        //adjust size and set layout
        this.setSize(415, 472);
        this.setLayout (null);

        //add components
        add (jsp);

        //set component bounds (only needed by Absolute Positioning)
        jsp.setBounds (0, 0, 400, 435);
        
        txtLog.setText("FORMAT EXAMPLE: \n > currentTime : maxTime \n Event\n=======================\n\n");
        
        this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        this.setLocation(dim.width/2+this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setTitle("Event Log");
        this.setVisible (true);
    }
	
	public static void setLog (final String txt){
		 SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                txtLog.setText( txtLog.getText() + txt );
	                
	            }
	        });

	}
}
