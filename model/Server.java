package ro.tuc.pt.asig2.model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import ro.tuc.pt.asig2.logic.Main;
import ro.tuc.pt.asig2.ui.LogFrame;


public class Server implements Runnable{

	public BlockingQueue<Task> tasks;
	public String name ="";
	public int maxWait=0;
	public int minWait=0;
	Thread t;
	
	public Server(String name){
		super();
		this.name = name;
		tasks = new LinkedBlockingQueue<Task>();
		t = new Thread(this);
	    t.start();
	}
	
	public void addTask(Task newTask) throws InterruptedException{
		tasks.add(newTask);
	}

	public void run() {
		while(true){
			try{
				Task task = tasks.take();
				Thread.sleep( (int) (task.getProcessingTime()*500) );
				if(!Main.running){
					break;
				}
				LogFrame.setLog("TASK["+task.getID()+"]"+" was completed by SERVER[" +name+"]\n");
			}catch (InterruptedException e) {
			// TODO Auto-generated catch block
				LogFrame.setLog(e.toString());
			}
		}
		
	}
	
	public int getWaitingTime(){
		int time = 0;
		for(int i=0; i<tasks.size(); i++){
			time+=getTasks()[i].getProcessingTime();
		}
		return time;
	}
	
	public Task[] getTasks(){
		Task[] array = new Task[tasks.size()];
		tasks.toArray(array);
		return array;
	}
	
	public String getStringTasks(){
		String st="";
		for(int i =0; i<getTasks().length; i++){
			st+=getTasks()[i].getString()+" | ";
		}
		return st;
	}
	
	
	public synchronized int getSize() throws InterruptedException{
		int size = tasks.size();
		return size;
	}

}
