package ro.tuc.pt.asig2.model;

public class Task{
	
	private long id;
	private int arrivalTime;
	private int processingTime;
	private int finishTime;
	
	public Task(long id, int arrivalTime, int processingTime){
		super();
		this.id = id;
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
		finishTime = arrivalTime + processingTime;
	}
	
	public long getID(){
		return id;
	}
	
	public int getArrivalTime(){
		return arrivalTime;
	}
	
	public int getProcessingTime(){
		return processingTime;
	}
	
	public int getFinishTime(){
		return finishTime;
	}
	
	public String getString(){
		String s = String.valueOf(id);
		return s;
	}
}
