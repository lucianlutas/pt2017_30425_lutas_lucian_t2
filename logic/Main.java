package ro.tuc.pt.asig2.logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ro.tuc.pt.asig2.ui.LogFrame;
import ro.tuc.pt.asig2.ui.SimulatorFrame;

public class Main {
	
	static SimulationManager gen;
	static Thread t;
	public static SimulatorFrame frame;
	public static LogFrame log;
	public static boolean running;
	
	public static void main(String[] args){
		frame = new SimulatorFrame();
		log = new LogFrame();
		btnStartActionListener();
		btnStopActionListener();
	}
	
	public static void btnStartActionListener (){
		SimulatorFrame.btnStart.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LogFrame.setLog("SIMULATION IS STARTING\n\n");
				running = true;
				gen = new SimulationManager(Integer.parseInt(SimulatorFrame.txtTime.getText()),Integer.parseInt(SimulatorFrame.txtServers.getText()), Integer.parseInt(SimulatorFrame.txtTasks.getText()));
				t = new Thread(gen);
				t.start();
				SimulatorFrame.btnStart.setEnabled(false);
				SimulatorFrame.btnStop.setEnabled(true);
			}
		});
	}
	public static void btnStopActionListener (){
		SimulatorFrame.btnStop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				running = false;
				SimulatorFrame.btnStart.setEnabled(true);
				SimulatorFrame.btnStop.setEnabled(false);
			}
		});
	}
	
}
