package ro.tuc.pt.asig2.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import ro.tuc.pt.asig2.model.Task;
import ro.tuc.pt.asig2.ui.LogFrame;
import ro.tuc.pt.asig2.ui.SimulatorFrame;

public class SimulationManager implements Runnable{

	public int timeLimit=100;
	public int maxProcessingTime = 10;
	public int numberOfServers;
	public int numberOfTasks;
	public Scheduler scheduler;
	public List<Task> generatedTasks;
	Random rand = new Random();
	
	public SimulationManager(int maxProcessingTime, int numberOfServers, int numberOfTasks){
		this.numberOfServers = numberOfServers;
		this.numberOfTasks = numberOfTasks;
		this.maxProcessingTime = maxProcessingTime;
		generatedTasks = new ArrayList<Task>();
		generateNRandomTasks(numberOfTasks);
		sortTasks();
		scheduler = new Scheduler(numberOfServers);
		
		timeLimit = getMaxProcTime();
	}
	
	public int getMaxProcTime(){
		int max=0;
		int temp=0;
		int sum = 0;
		for(int i =0; i<generatedTasks.size(); i++){
			temp=generatedTasks.get(i).getFinishTime();
			sum+=generatedTasks.get(i).getProcessingTime();
			if(temp>max){
				max = temp;
			}
		}
		return max+sum;
	}
	
	private void generateNRandomTasks( int n ){
		for(int i = 0; i<n; i++){
			Task task = new Task(i, rand.nextInt(timeLimit)+1, rand.nextInt(maxProcessingTime)+1);
			generatedTasks.add(task);
		}
	}
	
	private void sortTasks(){
		Collections.sort(generatedTasks, new Comparator<Task>(){
			   @Override
			   public int compare(Task t1, Task t2){
			        if(t1.getArrivalTime() < t2.getArrivalTime()){
			           return -1; 
			        }
			        if(t1.getArrivalTime() > t2.getArrivalTime()){
			           return 1; 
			        }
			        return 0;
			   }
			}); 
	}
	
	
	public void run() {
			try{
				int currentTime = 0;
				while(currentTime < timeLimit && Main.running){
					LogFrame.setLog("> "+currentTime+" : "+timeLimit+"\n");
					SimulatorFrame.setLog(getText(numberOfServers));
					int m = scheduler.min_index();
					if( !generatedTasks.isEmpty()){
						for(int i=0; i<generatedTasks.size(); i++){
							Task temp = generatedTasks.get(i);
							if(temp.getArrivalTime() == currentTime){
								generatedTasks.remove(i);
								LogFrame.setLog("TASK[" +temp.getID()+"]"+" was assigned to SERVER["+ Integer.toString(m)+"]\n");
								scheduler.getServers().get(m).addTask(temp);
								i--;
							}
						}
						
					}
					int test = 0;
					for(int i = 0; i<scheduler.getServers().size(); i++){
						if(scheduler.getServers().get(i).tasks.isEmpty()){
							test++;
							
						}
					}
					currentTime++;
					Thread.sleep(501);
					if(test == scheduler.getServers().size() && generatedTasks.isEmpty()){
						break;
					}
				}
				LogFrame.setLog("=======================\nEND OF SIMULATION\n");
				scheduler.maxWaitingTime();
				scheduler.minWaitingTime();
				SimulatorFrame.btnStart.setEnabled(true);
			} catch(InterruptedException e) {
				// TODO Auto-generated catch block
				LogFrame.setLog(e.toString());
				
			}
		}
		

	
	public String getText(int n){
		String s = "Incoming: ";
		for(int i=0; i<generatedTasks.size(); i++){
			s+=generatedTasks.get(i).getString()+" | ";
		}
		s += "\n\n";
		for(int i = 0; i<n; i++){
			s+= "Server "+ scheduler.getServers().get(i).name+ ": "+scheduler.getServers().get(i).getStringTasks()+"\n\n";
		}
		return s;
	}
	
}
