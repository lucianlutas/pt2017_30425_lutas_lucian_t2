package ro.tuc.pt.asig2.logic;

import java.util.*;

import ro.tuc.pt.asig2.model.Server;
import ro.tuc.pt.asig2.ui.LogFrame;
import ro.tuc.pt.asig2.ui.SimulatorFrame;


public class Scheduler{
	
	private List<Server> servers;
	private int maxNoServers;
	
	public Scheduler(int maxNoServers){
		servers = new ArrayList<Server>();
		for(int i=0; i<maxNoServers; i++){
			Server s = new Server(Integer.toString(i));
			servers.add(s);
		}
		this.maxNoServers = maxNoServers;
	}
	
	public List<Server> getServers(){
		return servers;
	}
	
	public void maxWaitingTime(){
		int index = 0;
		int max=0;
		int temp = servers.get(0).getWaitingTime();
		for( int i=1; i<maxNoServers; i++){
			max = servers.get(i).getWaitingTime();
			if ( max < temp ){
				temp = max;
				index = i;
			}
			servers.get(index).maxWait = max;
			LogFrame.setLog("SERVER["+index+"] max waiting time is "+servers.get(index).maxWait+"\n");
		}
	}
	
	public void minWaitingTime(){
		int index = 0;
		int min=0;
		int temp = servers.get(0).getWaitingTime();
		for( int i=1; i<maxNoServers; i++){
			min = servers.get(i).getWaitingTime();
			if ( min > temp ){
				temp = min;
				index = i;
			}
			servers.get(index).minWait = min;
			LogFrame.setLog("SERVER["+index+"] min waiting time is "+servers.get(index).minWait+"\n");
		}
	}
	
	public int min_index() {
		int index = 0;
		try{
			if(SimulatorFrame.cmbSel.getSelectedIndex() == 0){
				int min = servers.get(0).getSize();
				for( int i=1; i<maxNoServers; i++){
					int l = servers.get(i).getSize();
					if ( l < min ){
						min = l;
						index = i;
					}
				}
			} else{
				int min = servers.get(0).getWaitingTime();
				for( int i=1; i<maxNoServers; i++){
					int l = servers.get(i).getWaitingTime();
					if ( l < min ){
						min = l;
						index = i;
					}
				}
			}
		 }
		 catch( InterruptedException e ){
			 LogFrame.setLog(e.toString());
		 }
		 return index;
	}
	
}